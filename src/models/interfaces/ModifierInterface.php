<?php 

namespace Beweb\Td\Models\Interface;

interface ModifierInterface {

    function update(Updatable $stat): void;
}
