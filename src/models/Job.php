<?php

namespace Beweb\Td\Models;

abstract class Job {

  abstract function getModifPv(): int;
  abstract function getModifAtt(): int;
  abstract function getModifDef(): int;

}