<?php 

namespace Beweb\Td\Models;

class Stats {
    public int $pv;
    public int $def;
    public int $att;

    function __construct(){
        $this->pv = 0;
        $this->def = 0;
        $this->att = 0;
    }
    
}
